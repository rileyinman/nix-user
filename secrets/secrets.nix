let
  riley-carbide = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMzbRiue774qv3DVBSrZq4JrKYo/obX2254dDI2TYa8t";

  users = [
    riley-carbide
  ];
in
{
  "i3blocks/openweathermap.age".publicKeys = [
    riley-carbide
  ];

  "i3blocks/city_id.age".publicKeys = [
    riley-carbide
  ];
}
