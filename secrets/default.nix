{ pkgs, config, homeage, ... }:

{
  homeage = {
    pkg = pkgs.rage;
    isRage = true;
    identityPaths = [ "${config.home.homeDirectory}/.ssh/id_ed25519_age" ];
  };
}
