{ ... }:

{
  imports = [
    ../../general/dvd.nix
    ../../general/gui.nix
    ../../general/sound.nix
    ../../general/video-accel.nix

    ../../desktops/sway

    ../../programs/bitwarden.nix
    ../../programs/bluetooth.nix
    ../../programs/cli-utils.nix
    ../../programs/efi.nix
    ../../programs/gaming.nix
    ../../programs/github.nix
    ../../programs/media.nix
    ../../programs/office.nix
    ../../programs/recording.nix
    ../../programs/virtualization.nix

    ../../services/kdeconnect.nix
    ../../services/syncthing.nix
  ];

  wayland.windowManager.sway.config.output = {
    HDMI-A-1.pos = "0 760";
    DP-1.pos = "2560 760";
    HDMI-A-2 = {
      pos = "5120 0";
      transform = "90";
    };
  };
}
