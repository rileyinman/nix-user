{ ... }:

{
  imports = [
    ../../general/dvd.nix
    ../../general/gui.nix
    ../../general/sound.nix
    ../../general/video-accel.nix

    ../../desktops/sway

    ../../programs/android.nix
    ../../programs/art.nix
    ../../programs/bitwarden.nix
    ../../programs/bluetooth.nix
    ../../programs/cli-utils.nix
    ../../programs/efi.nix
    ../../programs/gaming.nix
    ../../programs/github.nix
    ../../programs/latex.nix
    ../../programs/media.nix
    ../../programs/office.nix
    ../../programs/pass.nix
    ../../programs/recording.nix
    ../../programs/rust.nix
    ../../programs/virtualization.nix
    ../../programs/weechat.nix

    ../../services/kdeconnect.nix
    ../../services/syncthing.nix
  ];

  wayland.windowManager.sway.config.input."2:10:TPPS/2_Elan_TrackPoint" = {
    events = "disabled";
    accel_profile = "adaptive";
    dwt = "enabled";
    pointer_accel = "-0.3";
  };
}
