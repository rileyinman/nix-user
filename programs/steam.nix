{ pkgs, unfreepkgs, ... }:

{
  home.packages = with unfreepkgs; let
    steam-bigpicture = writeShellScriptBin
      "steam-bigpicture"
      "${steam}/bin/steam -gamepadui";
  in [
    gnome3.zenity # necessary for some steam messages
    sc-controller
    unfreepkgs.steam
    steam-bigpicture
    steam-run
  ];

  # Enable Steam Deck UI in big picture mode
  home.file.".steam/steam/package/beta".text = ''
    steampal_stable_9a24a2bf68596b860cb6710d9ea307a76c29a04d
  '';
}
