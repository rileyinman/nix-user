{ pkgs, localpkgs, ... }:

let
  mytexlive.pkgs = [ localpkgs.texlive.mla-tex ];
in
{
  # TODO: Add APA6
  home.packages = with pkgs; [
    biber
    lmodern
  ];

  programs.texlive = {
    enable = true;
    extraPackages = tpkgs: {
      inherit (tpkgs)
      collection-latexrecommended
      collection-fontsextra
      latexmk
      lualibs
      xetex
      ;
      inherit mytexlive;
    };
  };
}
