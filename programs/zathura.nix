{ ... }:

{
  programs.zathura = {
    enable = true;
    options = {
      default-bg = "#353945";
      completion-bg = "#2f343f";
      completion-group-bg = "#262b36";
      completion-highlight-bg = "#5294e2";
      inputbar-bg = "#353945";
      inputbar-fg = "#d8d8d8";
      statusbar-bg = "#262b36";
    };
  };
}
