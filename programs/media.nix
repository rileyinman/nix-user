{ pkgs, upkgs, ... }:

{
  home.packages = [
    pkgs.plex-media-player
    upkgs.youtube-dl
  ];
}
