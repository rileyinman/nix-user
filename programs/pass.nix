{ pkgs, ... }:

{
  programs.password-store = {
    enable = true;
    package = pkgs.pass.withExtensions (exts: [
      exts.pass-audit
      exts.pass-genphrase
    ]);
  };

  services.password-store-sync.enable = true;
}
