{ upkgs, ... }:

{
  home.packages = with upkgs; [
    streamlink
    streamlink-twitch-gui-bin
  ];
}
