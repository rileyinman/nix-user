{ ... }:

{
  programs.mcfly = {
    enable = true;
    enableFuzzySearch = true;
  };
}
