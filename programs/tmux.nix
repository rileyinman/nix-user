{ pkgs, ... }:

{
  programs.tmux = {
    enable = true;
    shortcut = "t";
    baseIndex = 0;
    clock24 = true;
    customPaneNavigationAndResize = true;
    escapeTime = 0;
    historyLimit = 50000;
    keyMode = "emacs";
    newSession = true;

    # Upgrade terminal to use nicities like italics if possible
    terminal = "tmux-256color";

    extraConfig = /* ft=tmux */ ''
      set -g focus-events on
      set -g renumber-windows on
      set -g set-titles-string '#S:#I.#P #W'
      set -g status-right '#{?client_prefix,#[bg=yellow] Prefix #[bg=green],} | #H %a %m/%d %H:%M '
      set -g status-interval 5
      set -g mouse off
      set -g set-clipboard external

      bind C-p previous-window
      bind C-n next-window
    '';

    sensibleOnTop = false;
    plugins = with pkgs.tmuxPlugins; [
      sessionist
      {
        plugin = resurrect;
        extraConfig = ''
          set -g @resurrect-strategy-nvim 'session'
          set -g @resurrect-capture-pane-contents 'on'
        '';
      }
      {
        plugin = continuum;
        extraConfig = ''
          set -g @continuum-save-interval '15'
          set -g @continuum-restore 'on'
        '';
      }
    ];
  };
}
