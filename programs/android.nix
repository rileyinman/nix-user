{ unfreepkgs, ... }:

{
  home.packages = [ unfreepkgs.android-studio ];

  # Fix Android Studio not working in WMs
  home.sessionVariables._JAVA_AWT_WM_NONREPARENTING = 1;
}
