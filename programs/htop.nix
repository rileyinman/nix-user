{ ... }:

{
  programs.htop = {
    enable = true;
    settings = {
      left_meters = [ "LeftCPUs" "Memory" "Swap" ];
      right_meters = [ "RightCPUs" "Tasks" "LoadAverage" "Uptime" ];
    };
  };
}
