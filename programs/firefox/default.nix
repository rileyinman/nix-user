{ pkgs, hostname, ... }:

{
  home.file.".mozilla/firefox/riley/chrome" = {
    source = ./styles;
    recursive = true;
  };

  programs.firefox = {
    enable = true;
    package = pkgs.firefox;
    profiles.riley = {
      id = 0;
      isDefault = true;
      settings = {
        # Show bookmarks created on the mobile app
        "browser.bookmarks.showMobileBookmarks" = true;

        # Enable deprecated / unsupported compact UI
        "browser.compactmode.show" = true;

        # Tracking protection - blocks:
        #   Social media trackers
        #   Cross-site tracking cookies
        #   Tracking content in all windows
        #   Cryptominers
        #   Fingerprinters
        "browser.contentblocking.category" = "strict";

        # Ask where to download files
        "browser.download.useDownloadDir" = false;

        # Regional search results
        "browser.search.region" = "US";

        # Even if sessions are saved on quit, ask to quit anyways
        "browser.sessionstore.warnOnQuit" = true;

        # Restore previous sessions
        "browser.startup.page" = 3;

        # Leave the window open when closing the last tab
        "browser.tabs.closeWindowWithLastTab" = false;

        # ᴄᴏᴍᴘᴀᴄᴛ
        "browser.uidensity" = 1;

        # Don't connect to sites early when searching
        "browser.urlbar.speculativeConnect.enabled" = false;

        # Show the full url
        "browser.urlbar.trimURLs" = false;

        # I already know that extensions can be used in private mode,
        # so don't tell me again
        "extensions.privatebrowsing.notification" = true;

        # Not sure what these do?
        "extensions.ui.dictionary.hidden" = true;
        "extensions.ui.locale.hidden" = true;

        # Make sites work if they want Times New Roman
        "font.name.sans-serif.x-western" = "Liberation Sans";
        "font.name.serif.x-western" = "Liberation Serif";

        # Don't warn when going to about:config
        "browser.aboutConfig.showWarning" = false;

        # Allow XF86 media keys for pause/play
        "media.hardwaremediakeys.enabled" = true;

        # Don't allow the prefetching of DNS requests
        "network.dns.disablePrefetch" = true;

        # Allow the experimental http/3 protocol
        "network.http.http3.enabled" = true;

        # Don't predict what pages will be navigated to next
        "network.predictor.enabled" = false;

        # Don't prefetch pages marked by the current page
        "network.prefetch-next" = false;

        # Use encrypted SNI (doesn't work for now)
        "network.security.esni.enabled" = true;

        # Don't use Firefox's built in DoH tunnel,
        # as applications should not have control over DNS
        "network.trr.mode" = 5;

        # Always send 'do not track' header
        "privacy.donottrackheader.enabled" = true;

        # Enable 'container' tabs
        "privacy.userContext.enabled" = true;
        "privacy.userContext.ui.enabled" = true;

        # Set the machine's sync name
        "services.sync.client.name" = "Riley's Firefox on ${hostname}";
        "services.sync.username" = "rileyminman@gmail.com";

        # Don't ask to save passwords to Firefox
        "signon.rememberSignons" = false;

        # Automatically invert dark svg icons in dark mode
        "svg.context-properties.content.enabled" = true;

        # Allow the use of userContent.css and userChrome.css
        "toolkit.legacyUserProfileCustomizations.stylesheets" = true;

        # I've used Firefox before, don't show about:welcome
        "trailhead.firstrun.didSeeAboutWelcome" = true;
      };
    };

    profiles.media = {
      id = 1;
      settings = {
        # Even if sessions are saved on quit, ask to quit anyways
        "browser.sessionstore.warnOnQuit" = true;

        # Restore previous sessions
        "browser.startup.page" = 3;

        # Enable DRM content
        "media.eme.enabled" = true;

        # Don't ask to save passwords to Firefox
        "signon.rememberSignons" = false;

        # Allow links to open in new windows
        "browser.link.open_newwindow" = 2;

        # Apply this setting to all windows
        "browser.link.open_newwindow.restriction" = 0;

        # Open in new window even when middle clicking
        "browser.tabs.opentabfor.middleclick" = false;
      };
    };
  };
}
