{ ... }:

{
  imports = [
    ./firefox
    ./chromium.nix
    ./tor.nix
  ];
}
