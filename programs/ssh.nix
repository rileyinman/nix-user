{ ... }:

{
  programs.ssh = {
    enable = true;
    forwardAgent = true;
    extraConfig = /* ft=sshconfig */ ''
      AddKeysToAgent yes
    '';
  };
}
