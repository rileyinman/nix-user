{ ... }:

{
  imports = [
    ./dircolors.nix
    ./direnv.nix
    ./git.nix
    ./gpg.nix
    ./htop.nix
    ./neovim
    ./solokeys.nix
    ./ssh.nix
    ./tmux.nix
    ./zsh
  ];
}
