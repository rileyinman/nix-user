{ pkgs, unfreepkgs, upkgs, ... }:

{
  home.packages = with pkgs; [
    unfreepkgs.discord
    upkgs.discord-canary
    element-desktop
    mumble
    upkgs.syncplay
    upkgs.tdesktop
  ];
}
