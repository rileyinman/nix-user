{ pkgs, unfreepkgs, ... }:

{
  home.packages = with pkgs; [
    unfreepkgs.aseprite-unfree
    inkscape
    krita
  ];
}
