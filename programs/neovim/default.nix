{ pkgs, localpkgs, ... }:

{
  home.packages = [ pkgs.neovim-qt ];

  xdg.configFile.nvim = {
    source = ./files;
    recursive = true;
  };

  home.sessionVariables = {
    EDITOR = "${pkgs.neovim}/bin/nvim";
    DIFFPROG = "${pkgs.neovim}/bin/nvim -d";
    MANPAGER = "${pkgs.neovim}/bin/nvim +Man!";
  };

  programs.neovim = {
    enable = true;
    coc = {
      # enable = true;
      # settings = {};
    };

    viAlias = true;
    vimAlias = true;
    vimdiffAlias = true;

    withNodeJs = true;

    extraConfig = /* ft=vim */ ''
      " load filetype-based plugins
      filetype plugin on

      " unicode
      scriptencoding utf-8

      " enable filetype-based indentation
      filetype indent on

      " set other indentation to tab, 4-width
      set tabstop=4 shiftwidth=4 softtabstop=4 noexpandtab autoindent

      " show invisible characters
      set list listchars=tab:▸…,eol:¬,nbsp:%

      " disable default status bar
      set noshowmode

      " always show tab line
      set showtabline=2

      " show line numbers
      set relativenumber

      " column at line 80
      set colorcolumn=80

      " don't unload buffers when not visible
      set hidden

      " case-insensitive pathname tab completion
      set wildignorecase

      " persistent undo
      if !isdirectory($HOME.'/.cache/nvim')
        call mkdir($HOME.'/.cache/nvim')
      endif

      if !isdirectory($HOME.'/.cache/nvim/.undo')
        call mkdir($HOME.'/.cache/nvim/.undo')
      endif

      if !isdirectory($HOME.'/.cache/nvim/.backup')
        call mkdir($HOME.'/.cache/nvim/.backup')
      endif

      if !isdirectory($HOME.'/.cache/nvim/.swp')
        call mkdir($HOME.'/.cache/nvim/.swp')
      endif

      set undodir=$HOME/.cache/nvim/.undo//
      set backupdir=$HOME/.cache/nvim/.backup//
      set directory=$HOME/.cache/nvim/.swp//
      set undofile

      " better searching
      set ignorecase smartcase showmatch

      " allow concealing
      set conceallevel=2 concealcursor=niv

      " --- mappings ---
      " set space as leader
      let mapleader = ' '

      " navigate by visual lines by default
      nnoremap j gj
      vnoremap j gj
      nnoremap gj j
      vnoremap gj j
      nnoremap k gk
      vnoremap k gk
      nnoremap gk k
      vnoremap gk k

      " yank to end of line
      nnoremap Y y$

      " spellcheck
      map <silent> <F2> :setlocal spell! spelllang=en_us<CR>
      imap <sirent> <F2> <C-o>:setlocal spell! spelllang=en_us<CR>

      " navigate buffers
      nnoremap <C-n> :bn<CR>
      nnoremap <C-p> :bp<CR>

      " save file
      nnoremap <Leader>s :w<CR>

      " clear highlight
      nnoremap <C-l> :noh<CR><C-l>

      " --- commands ---
      " convert leading spaces to tabs
      command! -nargs=1 -range=% SpaceToTab <line1>,<line2>s/\v%(^ *)@<= {<args>}/\t/g
    '';

    plugins = with pkgs.vimPlugins; [
      # Theme
      {
        plugin = base16-vim;
        config = /* ft=vim*/ ''
          syntax enable
          set termguicolors
          colorscheme base16-eighties

          " TODO: Remove this duplicate config once
          " https://github.com/nix-community/home-manager/pull/2391 is merged
          let mapleader = ' '
        '';
      }

      # Replacement status + buffer bars
      {
         plugin = vim-airline;
         config = /* ft=vim*/ ''
           let g:airline_theme = 'base16'
           let g:airline#extensions#tabline#enabled = 1
           let g:airline#extensions#whitespace#mixed_indent_algo = 1
           let g:airline_highlighting_cache = 1
           let g:airline_skip_empty_sections = 1
         '';
      }
      vim-airline-themes

      # Tabs for indent, spaces for alignment
      localpkgs.vimPlugins.ingo-library
      {
         plugin = localpkgs.vimPlugins.IndentTab;
         config = "inoremap <silent> <expr> <Tab> IndentTab#Tab()";
      }

      # Reopen files at last edit position
      vim-lastplace

      # Clear highlight on cursor move
      localpkgs.vimPlugins.vim-evanesco

      # Linting and error checking
      # name = "vim-ale";

      # Completion
      # coc-nvim
      # coc-marketplace
      # coc-vimlsp
      # coc-json
      # coc-css
      # coc-stylelint
      # coc-java
      # coc-rls
      # coc-yaml
      # coc-python
      # coc-vimtex

      # neco-syntax
      # LanguageClient-neovim
      # deoplete-rust

      # Wrap/unwrap brackets
      {
         plugin = vim-argwrap;
         config = "nnoremap <silent> <leader>aw :ArgWrap<CR>";
      }

      # Add function endings automatically
      vim-endwise

      # Auto-pair brackets, quotes, etc.
      {
         plugin = auto-pairs;
         config = "let g:AutoPairsFlyMode = 1";
      }
      # coc-pairs

      # Align anything with ease
      {
         plugin = easy-align;
         config = /* ft=vim*/ ''
           xmap ga <Plug>(EasyAlign)
           nmap ga <Plug>(EasyAlign)
           nnoremap gA ga
         '';
      }

      # Snippets
      neosnippet neosnippet-snippets
      # coc-snippets
      # coc-ultisnips
      # coc-neosnippet

      # Version control status per line
      vim-signify

      # Search/jump by two chars
      vim-sneak

      # Unix command integration
      vim-eunuch

      # Git command integration
      vim-fugitive
      # coc-git

      # Surround targets
      vim-surround

      # Misc useful shortcuts
      vim-unimpaired

      # Comment/uncomment areas of text
      vim-commentary

      # Add a bunch of syntax types
      vim-polyglot

      # Latex compilation and live preview
      {
         plugin = vimtex;
         config = /* ft=vim*/ ''
           let g:tex_flavor = 'latex'
           let g:vimtex_view_method = 'zathura'
           let g:vimtex_imaps_enabled = 0
         '';
      }

      # Newer rust syntax
      rust-vim
    ];
  };
}
