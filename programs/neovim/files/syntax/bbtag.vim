if exists('b:current_syntax')
	finish
endif

if !exists('main_syntax')
	let main_syntax = 'bbtag'
endif

syn keyword simpleTags argsarray argslength commandname iscc lb rb semi zws contained
syn keyword generalTags abs args base radix base64decode atob base64encode btoa bool brainfuck capitalize choose clean color commit debug decrement delete embed embedbuild emoji fallback file for function func get hash if increment indexof length lock lower math max md5 min newline numformat parsefloat parseint randchoose randint randstr realpad regexreplace regexsplit regextest repeat loop replace request reverse rollback round rounddown floor roundup ceil set shuffle sleep space substring switch throw time trim upper uriencode void while apply contained
syn keyword arrayTags apply concat foreach isarray join jsonset pop push regexmatch match shift slice sort splice split contained
syn keyword blargTags dump exec execcc flag flagset inject modlog nsfw output pardon prefix quiet reason subtagexists suppresslookup timer waitmessage waitreaction waitreact warn warnings undefined contained
syn keyword discordTags ban channelcategories categories channelcategory category channelid categoryid channeliscategory iscategory channelisnsfw isnsfw channelistext istext channelisvoice isvoice channelname categoryname channelpos categorypos channels channeltype dm edit emojis guildbans guildcreatedat guildicon guildid guildmembers guildname guildownerid guildsize inguild isstaff ismod json j jsonget jget jsonstringify jstringify kick messageattachments attachments messageedittime messageembeds messageid messagesender sender messagetext text messagetime timestamp randuser reactadd addreact reactlist listreact reactremove removereact reactremoveall removereactall roleadd addrole rolecolor rolecreate roledelete roleid rolemembers rolemention rolename roleremove removerole roles rolesetmentionable rolesize inrole send slowmode unban useravatar usercreatedat userdiscrim usergame usergametype userhasrole userid userisbot userbot userjoinedat usermention username usernick userroles usersetnick setnick userstatus usertimezone webhook contained
syn keyword deprecatedTags pad lang contained

syn keyword bbBool true false
syn keyword bbStatement return contained

syn match  bbBraces    /\({\|}\)/
syn match  bbBrackets  /\(\[\|\]\)/ contained
syn match  bbCase      /\["\([^"]\+\)\?"\]/ contains=bbBrackets,bbString,bbSeparator
syn match  bbCase      /\[\d\+\]/ contains=bbBrackets,bbNumber,bbSeparator
syn match  bbCase      /\[\("\([^"]\+\)\?"\|\d\+\)\(,\s*\("\([^"]\+\)\?"\|\d\+\)\)\+\]/ contains=bbBrackets,bbNumber,bbString,bbSeparator
syn region bbComment   start=/{\/\/;/ end=/}/
syn match  bbOperator  /\(=\|!\|>\|<\)=/
syn match  bbNumber    /\d/
syn match  bbString    /".\{-}"/
syn match  bbSeparator /;/
syn match  bbSeparator /\./ contained
syn match  bbSeparator /,/ contained
syn match  bbTag       /{\w\+\(;\|}\)/ contains=simpleTags,generalTags,arrayTags,blargTags,discordTags,deprecatedTags,bbStatement,bbBraces,bbSeparator
syn match  bbTag       /{\w\+\./ contains=generalTags,bbBraces contained
syn match  bbFunction  /{func\(tion\)\=;\w\+;/ contains=bbBraces,bbSeparator,bbTag
syn match  bbFunction  /{func\.\w\+\(;\|}\)/ contains=bbBraces,bbSeparator,bbTag
syn match  bbVariable  /{\(get\|set\);\(_\|@\|\*\|\~\).\{-}\(;\|}\)/ contains=bbBraces,bbSeparator,bbTag
syn match  bbVariable  /{exec\(cc\)\=;\w\+\(;\|}\)/ contains=bbBraces,bbSeparator,bbTag
syn match  bbVariable  /{for;\(_\|@\|\*\|\~\).\{-};/ contains=bbBraces,bbSeparator,bbTag
syn match  bbVariable  /{foreach;\(_\|@\|\*\|\~\).\{-};\(_\|@\|\*\|\~\).\{-};/ contains=bbBraces,bbSeparator,bbTag
syn match  bbVariable  /{foreach;\(_\|@\|\*\|\~\).\{-};\[\("\([^"]\+\)\?"\|\d\+\)\]/ contains=bbBraces,bbCase,bbSeparator
syn match  bbVariable  /{foreach;\(_\|@\|\*\|\~\).\{-};\[\("\([^"]\+\)\?"\|\d\+\)\(,\s*\("\([^"]\+\)\?"\|\d\+\)\)\+\]/ contains=bbBraces,bbCase,bbSeparator,bbTag

syn match markdownItalic              /\*.\{-}\\\@!.\*/
syn match markdownItalic              /_.\{-}\\\@!._/
syn match markdownItalic              /\*_.\{-}\\\@!._\*/
syn match markdownItalic              /_\*.\{-}\\\@!.\*_/
syn match markdownBold                /\*\*.\{-}\\\@!.\*\*/
syn match markdownBoldItalic          /\*\*\*.\{-}\\\@!.\*\*\*/
syn match markdownUnderline           /__.\{-}\\\@!.__/
syn match markdownBoldUnderline       /\*\*__.\{-}\\\@!.__\*\*/
syn match markdownBoldUnderline       /__\*\*.\{-}\\\@!.\*\*__/
syn match markdownItalicUnderline     /\*__.\{-}\\\@!.__\*/
syn match markdownItalicUnderline     /__\*.\{-}\\\@!.\*__/
syn match markdownBoldItalicUnderline /\*\*\*__.\{-}\\\@!.__\*\*\*/
syn match markdownBoldItalicUnderline /__\*\*\*.\{-}\\\@!.\*\*\*__/

hi def link simpleTags     Function
hi def link generalTags    Function
hi def link arrayTags      Function
hi def link blargTags      Function
hi def link discordTags    Function
hi def link deprecatedTags Error

hi def link bbBool      Boolean
hi def link bbStatement Statement
hi def link bbBraces    Delimiter
hi def link bbBrackets  Delimiter
hi def link bbComment   Comment
hi def link bbOperator  Operator
hi def link bbNumber    Number
hi def link bbString    String
hi def link bbSeparator Special
hi def link bbFunction  Keyword
hi def link bbVariable  Keyword

hi markdownItalic              term=italic                cterm=italic                gui=italic
hi markdownBold                term=bold                  cterm=bold                  gui=bold
hi markdownBoldItalic          term=bold,italic           cterm=bold,italic           gui=bold,italic
hi markdownUnderline           term=underline             cterm=underline             gui=underline
hi markdownBoldUnderline       term=bold,underline        cterm=bold,underline        gui=bold,underline
hi markdownItalicUnderline     term=italic,underline      cterm=italic,underline      gui=italic,underline
hi markdownBoldItalicUnderline term=bold,italic,underline cterm=bold,italic,underline gui=bold,italic,underline

let b:current_syntax = 'bbtag'

if main_syntax == 'bbtag'
	unlet main_syntax
endif
