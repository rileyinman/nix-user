let g:nix_string_syntax_types = { "nix": { "syntax": "nixExpr" } }
let g:nix_string_syntax_simple_types = ["conf", "dnsmasq", "dosini", "gtkrc", "javascript", "modconf", "sh", "sshconfig", "sshdconfig", "tmux", "vim", "xf86conf"]

function! s:NixStringSyntax()
  let s:oldgroups = []
  let l:syntax = split(execute("syntax"), "\n")
  for l:line in l:syntax
    if l:line =~ "^[a-zA-Z]"
      let s:oldgroups += split(l:line)[0:0]
    endif
  endfor

  let l:newgroups = []

  for l:filetype in g:nix_string_syntax_simple_types
    call s:AddNixStringSyntax(l:filetype)
  endfor
  for [l:filetype, l:options] in items(g:nix_string_syntax_types)
    if has_key(l:options, "syntax")
      call s:AddNixStringSyntax(l:filetype, l:options["syntax"])
    else
      call s:AddNixStringSyntax(l:filetype)
    endif
    if has_key(l:options, "extraSetup")
      execute l:options["extraSetup"]
    endif
    if has_key(l:options, "extraGroups")
      let l:newgroups += l:options["extraGroups"]
    endif
  endfor

  let l:syntax = split(execute("syntax"), "\n")
  for l:line in l:syntax
    if l:line =~ "^[a-zA-Z]"
      let l:group = split(l:line)[0]
      if index(s:oldgroups, l:group) < 0
        let l:newgroups += [l:group]
      endif
    endif
  endfor
  if l:newgroups != []
    let l:containedinStr = " containedin=" . join(l:newgroups, ",")
  else
  let l:containedinStr = ""
  endif

  execute 'syntax region nixInterpolation matchgroup=nixInterpolationDelimiter start="\${" end="}" contained contains=@nixExpr,nixInterpolationParam extend'.l:containedinStr
  execute "syntax match nixStringSpecial /''['$]/ contained".l:containedinStr
  execute "syntax match nixStringSpecial /\\$\\$/ contained".l:containedinStr
  execute "syntax match nixStringSpecial /''\\\\[nrt]/ contained".l:containedinStr
  execute "syntax match nixInvalidStringEscape /''\\\\[^nrt]/ contained".l:containedinStr
endfunction

function! s:AddNixStringSyntax(filetype, ...)
  let l:ft = toupper(a:filetype)
  let l:ml = 'nixStringModeline'.l:ft
  let l:str = 'nixStringSyntax'.l:ft
  let s:oldgroups += [l:ml, l:str]

  if a:0 >= 1
    let l:syntax = '@'.a:1
  else
    if exists("b:current_syntax")
      unlet b:current_syntax
    endif
    let l:syntax = '@'.l:ft
    execute 'syntax include '.l:syntax.' syntax/'.a:filetype.'.vim'
    try
      execute 'syntax include '.l:syntax.' after/syntax/'.a:filetype.'.vim'
    catch
    endtry
    let b:current_syntax = 'nix'
  endif

  execute 'syntax match '.l:ml.'
        \ +/\* *ft='.a:filetype.' *\*/ *''+me=s
        \ skipwhite
        \ contained
        \ nextgroup='.l:str
  execute 'syntax region '.l:str."
        \ matchgroup=nixStringDelimiter
        \ start=+.*''+
        \ skip=+''['$\\\\]+
        \ end=+''+
        \ keepend
        \ contained
        \ contains=".l:syntax.",nixInterpolation,nixStringSpecial,nixInvalidStringEscape"
  execute 'syntax cluster nixExpr add='.l:ml
endfunction

call s:NixStringSyntax()
