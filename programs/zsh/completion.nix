{ ... }:

{
  programs.zsh.initExtra = /* ft=sh */ ''
    # Highlight currently selected completion
    zstyle ':completion:*' menu select

    # Pretty completion colors
    zstyle ':completion:*' list-colors "''${(@s.:.)LS_COLORS}"

    # Case-insensitive and smart-expanding matching
    zstyle ':completion:*' matcher-list 'm:{a-zA-Z-_}={A-Za-z_-}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'

    # Make ^W work better with paths
    backward-kill-custom() {
      local WORDCHARS="''${WORDCHARS//[\_\-\.\/]}"
      zle backward-kill-word
    }

    zle -N backward-kill-custom
    bindkey '^W' backward-kill-custom
  '';
}
