{ ... }:

{
  programs.zsh.initExtra = /* ft=sh */ ''
    # Create hash ("associative array")
    typeset -g -A key

    # Get available keys from our terminfo
    key[Home]="''${terminfo[khome]}"
    key[End]="''${terminfo[kend]}"
    key[Insert]="''${terminfo[kich1]}"
    key[Backspace]="''${terminfo[kbs]}"
    key[Delete]="''${terminfo[kdch1]}"
    key[Up]="''${terminfo[kcuu1]}"
    key[Down]="''${terminfo[kcud1]}"
    key[Left]="''${terminfo[kcub1]}"
    key[Right]="''${terminfo[kcuf1]}"
    key[PageUp]="''${terminfo[kpp]}"
    key[PageDown]="''${terminfo[knp]}"
    key[ShiftTab]="''${terminfo[kcbt]}"

    # Set up available keys
    [[ -n "''${key[Home]}" ]]      && bindkey -- "''${key[Home]}"      beginning-of-line
    [[ -n "''${key[End]}" ]]       && bindkey -- "''${key[End]}"       end-of-line
    [[ -n "''${key[Insert]}" ]]    && bindkey -- "''${key[Insert]}"    overwrite-mode
    [[ -n "''${key[Backspace]}" ]] && bindkey -- "''${key[Backspace]}" backward-delete-char
    [[ -n "''${key[Delete]}" ]]    && bindkey -- "''${key[Delete]}"    delete-char
    [[ -n "''${key[Up]}" ]]        && bindkey -- "''${key[Up]}"        up-line-or-history
    [[ -n "''${key[Down]}" ]]      && bindkey -- "''${key[Down]}"      down-line-or-history
    [[ -n "''${key[Left]}" ]]      && bindkey -- "''${key[Left]}"      backward-char
    [[ -n "''${key[Right]}" ]]     && bindkey -- "''${key[Right]}"     forward-char
    [[ -n "''${key[PageUp]}" ]]    && bindkey -- "''${key[PageUp]}"    history-beginning-search-backward
    [[ -n "''${key[PageDown]}" ]]  && bindkey -- "''${key[PageDown]}"  history-beginning-search-forward
    [[ -n "''${key[ShiftTab]}" ]]  && bindkey -- "''${key[ShiftTab]}"  reverse-menu-complete

    # Finally, make sure the terminal has valid terminfo codes
    if (( ''${+terminfo[smkx]} && ''${+terminfo[rmkx]} )); then
      autoload -Uz add-zle-hook-widget
      function zle_application_mode_start {
          echoti smkx
      }
      function zle_application_mode_stop {
          echoti rmkx
      }
      add-zle-hook-widget -Uz zle-line-init zle_application_mode_start
      add-zle-hook-widget -Uz zle-line-finish zle_application_mode_stop
    fi

    # Autosuggestions
    bindkey '^ ' autosuggest-accept
  '';
}
