{ lib, ... }:

{
  programs.zsh = {
    shellAliases = {
      # Make aliases work with sudo
      sudo = "sudo ";

      # Use nix-index integration if present, or cnf if not
      command-not-found = "command_not_found_handler";

      # Better lists
      ls = lib.mkDefault "ls --color";
      la = lib.mkDefault "ls -a";
      ll = lib.mkDefault "ls -l";
      lla = lib.mkDefault "ls -la";

      # Reduce overwriting mistakes
      rm = "rm -i";
      cp = "cp -i";
      mv = "mv -i";

      # Systemd
      sc = "systemctl";
      scu = "systemctl --user";

      # Git
      ga = "git add";
      gaa = "git add --all";
      gb = "git branch";
      gc = "git commit -v";
      gcam = "git commit -v --amend";
      gco = "git checkout";
      gcb = "git checkout -b";
      gcl = "git clone";
      gd = "git diff";
      gdc = "git diff --cached";
      gf = "git fetch";
      gl = "git pull";
      glo = "git log --oneline";
      gp = "git push";
      gpsup = "git push --set-upstream origin $(git_current_branch)";
      gr = "git remote";
      grv = "git remote -v";
      grh = "git reset";
      grhh = "git reset --hard";
      gst = "git status";
    };

    initExtra = /* ft=sh */ ''
      alias v='nvim'
      alias vd='nvim -d'
      alias vx='xv'
      xv() {
        if [[ -e "$1" ]]; then
          echo "Warning: file exists"
          sleep 1
        else
          touch "$1"
        fi

        chmod +x "$1"
        v "$1"
      }

      alias -g G='| grep'

      detach() {
        bg "%$1"
        disown "%$1"
      }

      mkcd() {
        mkdir -p "$1"
        cd "$1"
      }

      cht() {
        curl "https://cht.sh/$1"
      }

      git_current_branch() {
        local ref
        ref="$(command git symbolic-ref --quiet HEAD 2>/dev/null)"
        local ret="$?"
        if (( ret != 0 )); then
          (( ret == 128 )) && return
          ref="$(command git rev-parse --short HEAD 2>/dev/null)"
        fi
        echo "''${ref#refs/heads/}"
      }
    '';
  };
}
