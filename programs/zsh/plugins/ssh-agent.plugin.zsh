# Exit on non-interactive terminals
if ! [[ -t 1 ]]; then
	return
fi

# Exit if we don't have ssh-agent installed
if (( ! $+commands[ssh-agent] )); then
	return
fi

# If there is no agent, create one and mark it for deletion at exit
if [[ -z "$SSH_AUTH_SOCK" ]]; then
	eval "$(ssh-agent)" &>/dev/null
	zshexit() {
		echo "killing ssh agent $SSH_AGENT_PID"
		eval $(ssh-agent -k)
	}
fi
