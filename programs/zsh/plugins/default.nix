{ pkgs, localpkgs, ... }:

{
  # imports = [
  #   ./manydots-magic.nix
  #   ./git-escape-magic.nix
  # ];

  programs.zsh.plugins = with pkgs; [
    {
      name = "ssh-agent";
      src = ./.;
    }
    {
      name = "fast-syntax-highlighting";
      src = zsh-fast-syntax-highlighting.src;
    }
    {
      name = "git-escape-magic";
      file = "git-escape-magic";
      src = localpkgs.zsh-git-escape-magic.src;
    }
    {
      name = "manydots-magic";
      file = "manydots-magic";
      src = localpkgs.zsh-manydots-magic.src;
    }
    {
      name = "zsh-nix-shell";
      file = "nix-shell.plugin.zsh";
      src = zsh-nix-shell.src;
    }
    {
      name = "extract";
      file = "plugins/extract/extract.plugin.zsh";
      src = oh-my-zsh.src;
    }
    {
      name = "safe-paste";
      file = "plugins/safe-paste/safe-paste.plugin.zsh";
      src = oh-my-zsh;
    }
  ];

  programs.zsh.initExtraBeforeCompInit = /* ft=sh */ ''
    # Automatically escape pasted urls
    #   The function workaround makes it work with syntax highlighting
    autoload -Uz url-quote-magic
    function _url-quote-magic() { url-quote-magic; _zsh_highlight }
    zle -N self-insert _url-quote-magic
  '';
}
