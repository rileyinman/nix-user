{ ... }:

{
  imports = [
    ./aliases.nix
    ./bindings.nix
    ./completion.nix
    ./history.nix
    ./plugins
    ./theme
  ];

  programs.zsh = {
    enable = true;
    enableAutosuggestions = true;
    autocd = true;
    defaultKeymap = "emacs";

    initExtra = /* ft=sh */ ''
      # Don't make annoying noises
      unsetopt beep

      # More globbing features
      setopt extendedglob

      # Disable zsh handling bad matches
      setopt nomatch

      # Enable comments in zsh scripting
      setopt interactivecomments
    '';
  };
}
