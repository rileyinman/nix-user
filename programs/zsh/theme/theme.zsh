# Enable theming
setopt prompt_subst

# Set colors
local RED='%F{1}'
local CYAN='%F{6}'
local MAGENTA='%F{5}'
local BLUE='%F{33}'
local YELLOW='%F{221}'
local GREEN='%F{113}'
local RESET='%F{white}'

# Load version control info
autoload -Uz vcs_info
precmd() { vcs_info }
zstyle ':vcs_info:*' enable git

# Update after commands
zstyle ':vcs_info:*' check-for-changes true

# Show git revision
zstyle ':vcs_info:*' git-revision true

# Icons for changes
zstyle ':vcs_info:*' stagedstr '+ '
zstyle ':vcs_info:*' unstagedstr '* '

# Show branch and changes, with rebase shown if applicable
zstyle ':vcs_info:git:*' formats "${MAGENTA}%b${RESET} ${YELLOW}%u%c${RESET}"
zstyle ':vcs_info:git*' actionformats "${MAGENTA}%b${RESET} (${RED}%a${RESET}) ${YELLOW}%u%c${RESET}"

# Set color to red if error, green otherwise
local error="%(?.${GREEN}.${RED})"

# Username and hostname
local machine="${GREEN}%n@%m${RESET}"

# Current directory name
local current_dir="${CYAN}%1~${RESET}"

# Color arrow for last command
local dynamic_prompt="${error}-> ${RESET}"

if [[ -n "$IN_NIX_SHELL" ]]; then
  local nix_shell_indicator="${YELLOW}[nix-shell]${RESET} "
fi

# Put it all together with spacing
if [[ -n "$SSH_CLIENT" ]] || [[ -n "$SSH_TTY" ]]; then
  PROMPT='${machine} ${vcs_info_msg_0_}${current_dir}
${nix_shell_indicator}${dynamic_prompt}'
else
  PROMPT='${nix_shell_indicator}${vcs_info_msg_0_}${current_dir} ${dynamic_prompt}'
fi
