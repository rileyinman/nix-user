{ ... }:

{
  programs.zsh.initExtra = /* ft=sh */ ''
    source ${./theme.zsh}
  '';
}
