{ config, ... }:

{
  programs.zsh = {
    history = {
      path = "${config.xdg.cacheHome}/zsh/zsh_history";
      save = 10000000;
      size = 10000000;
    };

    initExtra = /* ft=sh */ ''
      # Exclude commands starting with a space from history
      setopt histignorespace

      if ! [[ -d "$HOME/.cache/zsh" ]]; then
        mkdir -p "$HOME/.cache/zsh"
      fi
    '';
  };
}
