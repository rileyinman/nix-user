{ ... }:

{
  imports = [ ./bat ];

  programs.git = {
    enable = true;
    delta.enable = true;

    userEmail = "rileyminman@gmail.com";
    userName = "Riley Inman";

    extraConfig = {
      push.default = "simple";
      pull.rebase = false;
      difftool.prompt = false;
      log.showSignature = true;

      delta = {
        syntax-theme = "base16-eighties";
      };

      sendemail = {
        from = "Riley Inman <rileyminman@gmail.com>";
        smtpserver = "smtp.gmail.com";
        stmpuser = "rileyminman@gmail.com";
        smtpencryption = "tls";
        smtpserverport = 587;
        chainreplyto = false;
      };

      url = {
        "git@github.com:".insteadOf = https://github.com/;
        "git@gitlab.com:".insteadOf = https://gitlab.com/;
      };
    };
  };
}
