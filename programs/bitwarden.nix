{ pkgs, upkgs, localpkgs, ... }:

{
  home.packages = with pkgs; [
    upkgs.bitwarden
    bitwarden-cli
    localpkgs.bitwarden-rofi
  ];
}
