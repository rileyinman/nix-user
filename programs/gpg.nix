{ ... }:

{
  imports = [ ../services/gpg.nix ];
  programs.gpg.enable = true;
}
