{ ... }:

{
  programs.rofi = {
    enable = true;
    theme = ./rofi.rasi;

    extraConfig = {
      font = "mono 10";
      modi = "run";
      display-run = "Run: ";
      show-icons = false;
      terminal = "alacritty";
      run-shell-command = "{terminal} -e {cmd}";
    };
  };
}
