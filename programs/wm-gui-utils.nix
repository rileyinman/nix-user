{ pkgs, ... }:

{
  home.packages = with pkgs; [
    mate.engrampa
    nomacs
    pcmanfm
    qalculate-gtk
  ];
}
