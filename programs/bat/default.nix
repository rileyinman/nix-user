{ lib, pkgs, ... }:

{
  programs.bat = {
    enable = true;
    themes.base16-eighties = builtins.readFile (pkgs.fetchurl {
      url = "https://raw.githubusercontent.com/chriskempson/base16-textmate/master/Themes/base16-eighties.tmTheme";
      hash = "sha256-aYn7mG+jzDxjRXsV9ZCwaqvNuTRfZlVm+XHPv/hAxOs=";
      postFetch = /* ft=sh */ ''
        patch -p1 $out < ${./base16-gutter.patch}
      '';
    });
    config.theme = "base16-eighties";
  };

  # Necessary to build bat themes automatically
  home.activation.bat-cache = lib.hm.dag.entryAfter ["writeBoundary"] /* ft=sh */ ''
    $DRY_RUN_CMD ${pkgs.bat}/bin/bat cache --build
  '';

  programs.zsh.shellAliases = {
    cat = "bat -p --paging=never";
    less = "bat --paging=always";
  };
}
