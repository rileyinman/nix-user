{ pkgs, ... }:

{
  home.packages = [ pkgs.blueman ];
  services.mpris-proxy.enable = true; # For bluetooth headphone media controls
}
