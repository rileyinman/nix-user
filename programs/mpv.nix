{ pkgs, localpkgs, ... }:

{
  programs.mpv = {
    enable = true;
    config = {
      ytdl-format = "bestvideo[height<=?1080]+bestaudio/best";
      cache = "yes";
      demuxer-max-bytes = 600000000;
      demuxer-max-back-bytes = 200000000;
    };

    scripts = with pkgs.mpvScripts; [
      localpkgs.mpvScripts.autocrop
      localpkgs.mpvScripts.autodeint
      localpkgs.mpvScripts.autosub
      localpkgs.mpvScripts.reload
      autoload
      sponsorblock
    ];
  };
}
