{ pkgs, lib, config, homeage, ... }:

{
  imports = [ ../jq.nix ];

  services.playerctld.enable = true;

  home.packages = with pkgs; [
    acpi
    bc
    brightnessctl
    i3blocks
    iw
    playerctl
    tlp
    wget
  ];

  homeage.file."i3blocks-openweathermap" = {
    source = ../../secrets/i3blocks/openweathermap.age;
    symlinks = [ "${config.xdg.configHome}/i3blocks/openweathermap.txt" ];
  };

  homeage.file."i3blocks-city_id" = {
    source = ../../secrets/i3blocks/city_id.age;
    symlinks = [ "${config.xdg.configHome}/i3blocks/city_id.txt" ];
  };

  xdg.configFile.i3blocks = {
    source = ./config;
    recursive = true;
  };
}
