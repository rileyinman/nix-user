{ ... }:

{
  imports = [
    ./browsers.nix
    ./mpv.nix
    ./music.nix
    ./social.nix
    ./twitch.nix
    ./zathura.nix
  ];
}
