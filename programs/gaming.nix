{ pkgs, localpkgs, ... }:

{
  imports = [ ./steam.nix ];

  home.packages = with pkgs; [
    localpkgs.custom-install
    moonlight-qt
    localpkgs.nxci
    localpkgs.ns-usbloader
    localpkgs.nsz
  ];
}
