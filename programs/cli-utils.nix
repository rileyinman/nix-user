{ pkgs, localpkgs, ... }:

{
  imports = [
    ./bat
    ./exa.nix
    ./lesspipe.nix
    ./mcfly.nix
    # ./navi.nix TODO: Enable once backported in home-manager
    ./nix-index.nix
  ];

  home.packages = with pkgs; [
    dnsutils
    fd
    file
    inetutils
    localpkgs.jqq
    libqalculate
    lm_sensors
    neofetch
    nethogs
    p7zip
    localpkgs.rural
    ripgrep
    localpkgs.toluol
    tree
    unar
    unzip
    usbutils
    xdg_utils
    zip
  ];

  programs.zsh.shellAliases = {
    dig = "toluol @127.0.0.53 +verbose";
  };
}
