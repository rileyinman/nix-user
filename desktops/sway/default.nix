{ config, lib, pkgs, unfreepkgs, ... }:

let
  bg-color = "#353945";
  medium-bg-color = "#2f343f";
  dark-bg-color = "#262b36";
  text-color = "#cad1da";
  inactive-text-color = "#727784";
  inactive-bg-color = "${bg-color}";
  urgent-bg-color = "#f04a50";
  indicator-color = "#0080ff";

  workspace1 = "1 ";
  workspace2 = "2 ";
  workspace3 = "3 ";
  workspace4 = "4 ";
  workspace5 = "5 ";
  workspace6 = "6 ";
  workspace7 = "7 ";
  workspace8 = "8 ";
  workspace9 = "9 ";
  workspace10 = "10 ";
  workspace11 = "11 ";
  workspace12 = "12 ";
  workspace13 = "13";
  workspace14 = "14";
  workspace15 = "15";
  workspace16 = "16";
  workspace17 = "17";
  workspace18 = "18";
  workspace19 = "19";
  workspace20 = "20";

  resizesym = "";

  blocks-reload = "pkill -RTMIN+10 i3blocks";

  swaymsg = "${pkgs.sway}/bin/swaymsg";

  selection = pkgs.writeShellScript
    "get-selection"
    "${swaymsg} -t get_tree | ${pkgs.jq}/bin/jq -r '.. | select(.pid? and .visible?) | .rect | \"\\(.x),\\(.y) \\(.width)x\\(.height)\"' | ${pkgs.slurp}/bin/slurp";

  screenshot = "${pkgs.grim}/bin/grim -g \"$(${selection})\"";

  pw-get-key = keyname: "${pkgs.pipewire}/bin/pw-dump | ${pkgs.jq}/bin/jq '.[] | select(.type == \"PipeWire:Interface:Metadata\") .metadata[] | select(.key == \"${keyname}\") .value .name'";

  get-default-sink = pw-get-key "default.audio.sink";
  get-default-source = pw-get-key "default.audio.source";

  set-wallpaper = pkgs.writeShellScriptBin "set-wallpaper" /* ft=sh */ ''
    current_time=$(( 10#$(${pkgs.coreutils}/bin/date '+%H%M') ))
    (( current_time > 0600 && current_time < 2000 )) \
      && wallpaper='firewatch' \
      || wallpaper='firewatch-night'

    display=$(${swaymsg} -t get_outputs | ${pkgs.jq}/bin/jq '.[] | .name' | ${pkgs.coreutils}/bin/head -1)
    ${swaymsg} output "$display" bg "${../../files/wallpapers}/$wallpaper.png" fill
  '';

  # This is needed in order to set items such as the cursor theme, because for
  # whatever godawful reason GTK pulls from org.gnome.desktop.interface via
  # gsettings instead of reading the config file on Wayland
  # NOTE: Remember to manually bump this once gtk-3.0 is no longer mainline
  import-gsettings = let
    grep = "${pkgs.gnugrep}/bin/grep";
    sed = "${pkgs.gnused}/bin/sed";
    gsettings = "${pkgs.glib.bin}/bin/gsettings";
  in pkgs.writeShellScriptBin "import-gsettings" /* ft=sh */ ''
    config="${config.xdg.configHome}/gtk-3.0/settings.ini"
    if [ ! -f "$config" ]; then exit 1; fi

    gnome_schema="org.gnome.desktop.interface"
    gtk_theme="$(${grep} 'gtk-theme-name' "$config" | ${sed} 's/.*\s*=\s*//')"
    icon_theme="$(${grep} 'gtk-icon-theme-name' "$config" | ${sed} 's/.*\s*=\s*//')"
    cursor_theme="$(${grep} 'gtk-cursor-theme-name' "$config" | ${sed} 's/.*\s*=\s*//')"
    font_name="$(${grep} 'gtk-font-name' "$config" | ${sed} 's/.*\s*=\s*//')"
    ${gsettings} set "$gnome_schema" gtk-theme "$gtk_theme"
    ${gsettings} set "$gnome_schema" icon-theme "$icon_theme"
    ${gsettings} set "$gnome_schema" cursor-theme "$cursor_theme"
    ${gsettings} set "$gnome_schema" font-name "$font_name"
  '';
in
{
  imports = [
    ../../general/gtk.nix
    ../../general/xdg.nix
    ../../programs/alacritty.nix
    ../../programs/gui.nix
    ../../programs/i3blocks
    ../../programs/jq.nix
    ../../programs/rofi
    ../../programs/wm-gui-utils.nix
    ../../services/gammastep.nix
    ../../services/kanshi.nix
    ../../services/mako.nix
    ../../services/udiskie.nix
  ];

  nixpkgs.overlays = [( import ../../nix-overlays/wayland.nix )];

  programs.zsh.loginExtra = /* ft=sh */ ''
    if [[ -z "$WAYLAND_DISPLAY" ]] && [[ "$(tty)" == /dev/tty1 ]]; then
      exec sway
    fi
  '';

  home.packages = with pkgs; [
    (buildEnv {
      name = "sway-scripts";
      paths = [(runCommand "sway-scripts" {} ''
        mkdir -p $out/bin
        cp ${./scripts}/* $out/bin/
      '')];
    })

    # clipman
    wl-clipboard
  ];

  # services.swayidle = {
  #   enable = true;
  #   events = [
  #     { event = "before-sleep"; command = "${pkgs.swaylock}/bin/swaylock"; }
  #   ];
  #   timeouts = [
  #     { timeout = 300; command = "${pkgs.swaylock}/bin/swaylock"; }
  #     {
  #       timeout = 600;
  #       command = "${swaymsg} \"output * dpms off\"";
  #       resumeCommand = "${swaymsg} \"output * dpms on\"";
  #     }
  #   ];
  # };

  wayland.windowManager.sway = {
    enable = true;
    systemdIntegration = true;
    wrapperFeatures.gtk = true;
    swaynag = {
      enable = true;
      settings."<config>".background = "ab4642";
    };
  };

  wayland.windowManager.sway.config = rec {
    defaultWorkspace = "workspace ${workspace1}";
    terminal = "${pkgs.alacritty}/bin/alacritty";
    modifier = "Mod4";

    left = "h";
    right = "l";
    up = "k";
    down = "j";

    focus = {
      followMouse = true;
      wrapping = "workspace";
    };

    fonts = {
      names = [ "sans" ];
      size = 11.0;
    };

    colors.focused = {
      background = "${bg-color}";
      border = "${bg-color}";
      childBorder = "${bg-color}";
      indicator = "${indicator-color}";
      text = "${text-color}";
    };

    colors.focusedInactive = {
      background = "${inactive-bg-color}";
      border = "${inactive-bg-color}";
      childBorder = "${bg-color}";
      indicator = "${indicator-color}";
      text = "${inactive-text-color}";
    };

    colors.unfocused = {
      background = "${inactive-bg-color}";
      border = "${inactive-bg-color}";
      childBorder = "${bg-color}";
      indicator = "${indicator-color}";
      text = "${inactive-text-color}";
    };

    colors.urgent = {
      background = "${urgent-bg-color}";
      border = "${urgent-bg-color}";
      childBorder = "${bg-color}";
      indicator = "${indicator-color}";
      text = "${text-color}";
    };

    keybindings = lib.mkOptionDefault {
      # Terminal
      "${modifier}+Shift+Return" = "exec ${terminal} -e ${pkgs.tmux}/bin/tmux";
      "${modifier}+Control+Shift+Return" = "exec ${terminal} --class FLOAT -e ${pkgs.tmux}/bin/tmux attach";

      # Rofi
      "${modifier}+e" = "exec ${pkgs.rofi}/bin/rofi -show run";
      "${modifier}+Shift+e" = "exec ${pkgs.pass}/bin/passmenu -p 'pass:'";

      # Layout
      "${modifier}+d" = "split h";
      "${modifier}+v" = "split v";
      "${modifier}+o" = "layout stacking";
      "${modifier}+comma" = "layout tabbed";
      "${modifier}+period" = "layout toggle split";

      # Manage floating windows
      "${modifier}+space" = "focus mode_toggle";
      "${modifier}+Shift+space" = "floating toggle";

      # Containers
      "${modifier}+a" = "focus parent";

      # Fullscreen
      "${modifier}+f" = "fullscreen toggle";

      # Pop out and sticky (good for video)
      "${modifier}+y" = "fullscreen disable; floating enable; resize set 800 600; sticky enable";

      # Toggle sticky
      "${modifier}+Control+s" = "sticky toggle";

      # Workspaces
      "${modifier}+1" = "workspace ${workspace1}";
      "${modifier}+2" = "workspace ${workspace2}";
      "${modifier}+3" = "workspace ${workspace3}";
      "${modifier}+4" = "workspace ${workspace4}";
      "${modifier}+5" = "workspace ${workspace5}";
      "${modifier}+6" = "workspace ${workspace6}";
      "${modifier}+7" = "workspace ${workspace7}";
      "${modifier}+8" = "workspace ${workspace8}";
      "${modifier}+9" = "workspace ${workspace9}";
      "${modifier}+0" = "workspace ${workspace10}";
      "${modifier}+Control+1" = "workspace ${workspace11}";
      "${modifier}+Control+2" = "workspace ${workspace12}";
      "${modifier}+Control+3" = "workspace ${workspace13}";
      "${modifier}+Control+4" = "workspace ${workspace14}";
      "${modifier}+Control+5" = "workspace ${workspace15}";
      "${modifier}+Control+6" = "workspace ${workspace16}";
      "${modifier}+Control+7" = "workspace ${workspace17}";
      "${modifier}+Control+8" = "workspace ${workspace18}";
      "${modifier}+Control+9" = "workspace ${workspace19}";
      "${modifier}+Control+0" = "workspace ${workspace20}";

      "${modifier}+Shift+1" = "move container to workspace ${workspace1}";
      "${modifier}+Shift+2" = "move container to workspace ${workspace2}";
      "${modifier}+Shift+3" = "move container to workspace ${workspace3}";
      "${modifier}+Shift+4" = "move container to workspace ${workspace4}";
      "${modifier}+Shift+5" = "move container to workspace ${workspace5}";
      "${modifier}+Shift+6" = "move container to workspace ${workspace6}";
      "${modifier}+Shift+7" = "move container to workspace ${workspace7}";
      "${modifier}+Shift+8" = "move container to workspace ${workspace8}";
      "${modifier}+Shift+9" = "move container to workspace ${workspace9}";
      "${modifier}+Shift+0" = "move container to workspace ${workspace10}";
      "${modifier}+Control+Shift+1" = "move container to workspace ${workspace11}";
      "${modifier}+Control+Shift+2" = "move container to workspace ${workspace12}";
      "${modifier}+Control+Shift+3" = "move container to workspace ${workspace13}";
      "${modifier}+Control+Shift+4" = "move container to workspace ${workspace14}";
      "${modifier}+Control+Shift+5" = "move container to workspace ${workspace15}";
      "${modifier}+Control+Shift+6" = "move container to workspace ${workspace16}";
      "${modifier}+Control+Shift+7" = "move container to workspace ${workspace17}";
      "${modifier}+Control+Shift+8" = "move container to workspace ${workspace18}";
      "${modifier}+Control+Shift+9" = "move container to workspace ${workspace19}";
      "${modifier}+Control+Shift+0" = "move container to workspace ${workspace20}";

      "${modifier}+grave" = "workspace back_and_forth";
      "${modifier}+Shift+grave" = "move container to workspace back_and_forth";

      "${modifier}+bracketleft" = "focus output left";
      "${modifier}+bracketright" = "focus output right";
      "${modifier}+Shift+bracketleft" = "exec ${swaymsg} move workspace to output left && ${swaymsg} focus output left";
      "${modifier}+Shift+bracketright" = "exec ${swaymsg} move workspace to output right && ${swaymsg} focus output right";

      "${modifier}+m" = "workspace prev";
      "${modifier}+w" = "workspace next";
      "${modifier}+Shift+m" = "move container to workspace prev";
      "${modifier}+Shift+w" = "move container to workspace next";

      # Volume
      "--locked XF86AudioMute" = "exec ${pkgs.pulseaudio}/bin/pactl set-sink-mute @DEFAULT_SINK@ toggle && ${blocks-reload}";
      "--locked XF86AudioMicMute" = "exec ${pkgs.pulseaudio}/bin/pactl set-source-mute @DEFAULT_SOURCE@ toggle && ${blocks-reload}";

      "--locked XF86AudioLowerVolume" = "exec ${pkgs.pulseaudio}/bin/pactl set-sink-mute @DEFAULT_SINK@ false && ${pkgs.pulseaudio}/bin/pactl set-sink-volume @DEFAULT_SINK@ -5% && ${blocks-reload}";
      "--locked XF86AudioRaiseVolume" = "exec ${pkgs.pulseaudio}/bin/pactl set-sink-mute @DEFAULT_SINK@ false && ${pkgs.pulseaudio}/bin/pactl set-sink-volume @DEFAULT_SINK@ +5% && ${blocks-reload}";
      "--locked Shift+XF86AudioLowerVolume" = "exec ${pkgs.pulseaudio}/bin/pactl set-sink-mute @DEFAULT_SINK@ false && ${pkgs.pulseaudio}/bin/pactl set-sink-volume @DEFAULT_SINK@ -1% && ${blocks-reload}";
      "--locked Shift+XF86AudioRaiseVolume" = "exec ${pkgs.pulseaudio}/bin/pactl set-sink-mute @DEFAULT_SINK@ false && ${pkgs.pulseaudio}/bin/pactl set-sink-volume @DEFAULT_SINK@ +1% && ${blocks-reload}";

      # Audio controls
      "--locked XF86AudioPlay" = "exec ${pkgs.playerctl}/bin/playerctl play-pause";
      "--locked XF86AudioPrev" = "exec ${pkgs.playerctl}/bin/playerctl previous";
      "--locked XF86AudioNext" = "exec ${pkgs.playerctl}/bin/playerctl next";

      # Brightness
      "--locked XF86MonBrightnessUp" = "exec ${pkgs.brightnessctl}/bin/brightnessctl s +5% && ${blocks-reload}";
      "--locked XF86MonBrightnessDown" = "exec ${pkgs.brightnessctl}/bin/brightnessctl s 5%- && ${blocks-reload}";
      "--locked Shift+XF86MonBrightnessUp" = "exec ${pkgs.brightnessctl}/bin/brightnessctl s +1% && ${blocks-reload}";
      "--locked Shift+XF86MonBrightnessDown" = "exec ${pkgs.brightnessctl}/bin/brightnessctl s 1%- && ${blocks-reload}";

      # Dismiss notifications
      "${modifier}+n" = "exec ${pkgs.mako}/bin/makoctl dismiss -a";

      # Scratchpad
      "${modifier}+apostrophe" = "scratchpad show";
      "${modifier}+Shift+apostrophe" = "move scratchpad";

      # Lock screen
      "${modifier}+Shift+minus" = "exec ${pkgs.swaylock}/bin/swaylock";

      # Exit
      "${modifier}+Shift+x" = "exec ${pkgs.sway}/bin/swaynag -t warning -m 'Do you want to exit?' -b 'Yes' '${swaymsg} exit'";

      # Screenshots
      "Print" = "exec ${screenshot} - | ${pkgs.wl-clipboard}/bin/wl-copy";
      "Control+Print" = "exec ${screenshot} ${config.home.homeDirectory}/pic/screenshots/$(date \"+%F-%T\").png";

      # Mode switch
      "${modifier}+r" = "mode \"${resizesym}\"";
    };

    modes."${resizesym}" = {
      "${left}" = "resize shrink width 8 px or 8 ppt";
      "Left" = "resize shrink width 8 px or 8 ppt";
      "${down}" = "resize grow height 8 px or 8 ppt";
      "Down" = "resize grow height 8 px or 8 ppt";
      "${up}" = "resize shrink height 8 px or 8 ppt";
      "Up" = "resize shrink height 8 px or 8 ppt";
      "${right}" = "resize grow width 8 px or 8 ppt";
      "Right" = "resize grow width 8 px or 8 ppt";

      "Shift+${left}" = "resize shrink width 1 px or 1 ppt";
      "Shift+Left" = "resize shrink width 1 px or 1 ppt";
      "Shift+${down}" = "resize grow height 1 px or 1 ppt";
      "Shift+Down" = "resize grow height 1 px or 1 ppt";
      "Shift+${up}" = "resize shrink height 1 px or 1 ppt";
      "Shift+Up" = "resize shrink height 1 px or 1 ppt";
      "Shift+${right}" = "resize grow width 1 px or 1 ppt";
      "Shift+Right" = "resize grow width 1 px or 1 ppt";

      "Return" = "mode \"default\"";
      "Escape" = "mode \"default\"";
      "${modifier}+r" = "mode \"default\"";
    };

    gaps.inner = 5;
    window.border = 0;

    bars = [{
      position = "top";
      statusCommand = "${pkgs.i3blocks}/bin/i3blocks";
      fonts = {
        names = [ "sans" "monospace" ];
        size = 10.0;
      };
      colors = {
        background = "${bg-color}";
        focusedWorkspace = {
          background = "${bg-color}";
          border = "${bg-color}";
          text = "${text-color}";
        };
        inactiveWorkspace = {
          background = "${bg-color}";
          border = "${inactive-bg-color}";
          text = "${inactive-text-color}";
        };
        urgentWorkspace = {
          background = "${urgent-bg-color}";
          border = "${urgent-bg-color}";
          text = "${text-color}";
        };
      };
      # TODO: Add config item for this
      extraConfig = ''
        height 25
        '';
    }];

    window.commands = [
      {
        command = "inhibit_idle fullscreen";
        criteria = { class = "^.*"; };
      }
      {
        command = "inhibit_idle fullscreen";
        criteria = { app_id = "^.*"; };
      }
    ];

    floating.criteria = [
      { instance = "^FLOAT$"; }
      { app_id = "qalculate-gtk"; }
      { app_id = "ibus-extension-gtk3"; }
    ];

    assigns = {
      "${workspace1}" = [{ app_id = "firefox"; }];
      "${workspace9}" = [
        { class = "discord"; }
        { class = "Element"; }
        { class = "Ferdi"; }
        { app_id = "telegramdesktop"; }
      ];
      "${workspace10}" = [
        { app_id = ".blueman-manager-wrapped"; }
        { app_id = "com.github.wwmm.easyeffects"; }
        { app_id = "mpv"; }
        { app_id = "pavucontrol"; }
      ];
      "${workspace11}" = [{ class = "Steam"; }];
      "${workspace12}" = [{ class = "transmission"; }];
    };

    startup = [
      # Lock screen
      # TODO: Replace with timeouts config
      {
        command = ''
          ${pkgs.swayidle}/bin/swayidle \
            timeout 300 '${pkgs.swaylock}/bin/swaylock' \
            timeout 600 '${swaymsg} "output * dpms off"' \
            resume '${swaymsg} "output * dpms on"' \
            before-sleep '${pkgs.swaylock}/bin/swaylock'
        '';
      }

      # GUI applications
      { command = "${pkgs.firefox}/bin/firefox"; }
      { command = "${unfreepkgs.discord}/bin/discord"; }
      { command = "${pkgs.element-desktop}/bin/element-desktop"; }
      { command = "${pkgs.tdesktop}/bin/telegram-desktop"; }
      { command = "${unfreepkgs.steam}/bin/steam"; }

      # Wallpaper
      { command = "${set-wallpaper}/bin/set-wallpaper"; always = true; }

      # Pick up some GTK settings
      { command = "${import-gsettings}/bin/import-gsettings"; always = true; }

      # Polkit agent
      { command = "${pkgs.lxqt.lxqt-policykit}/bin/lxqt-policykit-agent"; }

      # Notification daemon
      { command = "${pkgs.mako}/bin/mako"; }

      # Input method
      { command = "${pkgs.ibus}/bin/ibus-daemon --xim"; }

      # Clipboard
      # TODO: Make sure using pass doesn't copy into here
      # { command = "${pkgs.wl-clipboard}/bin/wl-paste -t text --watch ${pkgs.clipman}/bin/clipman store"; }
      # { command = "${pkgs.clipman}/bin/clipman restore"; }
    ];

    input = {
      "type:touchpad" = {
        accel_profile = "flat";
        dwt = "disabled";
        middle_emulation = "enabled";
        natural_scroll = "enabled";
        tap = "enabled";
        pointer_accel = "1";
      };

      "*" = {
        xkb_layout = "dvorak";
        xkb_options = "ctrl:nocaps,compose:rctrl";
      };
    };
  };

  xdg.configFile."swaylock/config".source = ./swaylock.conf;

  systemd.user.services.set-wallpaper = {
    Unit.Description = "Set wallpaper based on time";
    Service.Type = "oneshot";
    Service.ExecStart = "${set-wallpaper}/bin/set-wallpaper";
    Install.WantedBy = [ "sway-session.target" ];
  };

  systemd.user.timers.set-wallpaper = {
    Unit.Description = "Set wallpaper based on time";
    Timer.OnCalendar = "*-*-* 06,20:00:00";
    Timer.Persistent = true;
    Install.WantedBy = [ "default.target" ];
  };
}
