{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.05";
    nixpkgs-unstable.url = "github:NixOS/nixpkgs/nixos-unstable";

    # super-unstable.url = "path:/home/riley/code/nixpkgs";

    nur.url = "github:nix-community/NUR";

    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs.nixpkgs.follows = "nixpkgs-unstable";
    };

    home-manager = {
      url = "github:nix-community/home-manager/release-23.05";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    homeage = {
      url = "github:jordanisaacs/homeage/main";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = inputs@{ self
    , nixpkgs
    , nixpkgs-unstable
    , nur
    , rust-overlay
    , home-manager
    , homeage
    , ... }:
  let
    inherit (nixpkgs) lib;
    inherit (builtins) readDir attrNames mapAttrs;
    inherit (lib) const fileContents filterAttrs hasAttr mapAttrs' mkIf nameValuePair recursiveUpdate;

    machines = let
      username = "riley";
      getProps = name: let
        dirPath = ./machines + "/${name}";
        dirContents = readDir dirPath;
      in recursiveUpdate {
        inherit username;
        system = "x86_64-linux";
        files = mapAttrs (file: _: dirPath + "/${file}") dirContents;
        stateVersion = "23.05";
      } (if hasAttr "default.nix" dirContents
          then import dirPath
          else {});
    in mapAttrs' (name: _: nameValuePair "${username}@${name}" (getProps name)) (filterAttrs (k: v: v == "directory") (readDir ./machines));

    mkConfig = name: props: rec {
      pkgs = nixpkgs.legacyPackages.x86_64-linux;

      # Make inputs available as needed
      extraSpecialArgs = {
	inherit nixpkgs-unstable nur rust-overlay homeage;
        # inherit super-unstable;
      };

      modules = let 
        inherit (props) system stateVersion username;
      in [
        # Make processed inputs and variables available as needed
        {
          _module.args = let
            pkgs = nixpkgs.legacyPackages.x86_64-linux;
            unfreepkgs = import nixpkgs {
              inherit system;
              config = import ./config.nix;
            };
            upkgs = import nixpkgs-unstable {
              inherit system;
              config = import ./config.nix;
            };
          in {
            inherit unfreepkgs upkgs;

            hostname = name;

            localpkgs = rec {
              # mpvScripts
              # TODO: https://github.com/NixOS/nixpkgs/pull/161599
              mpvScripts = {
                autocrop = pkgs.callPackage ./pkgs/mpvScripts/autocrop {};
                autodeint = pkgs.callPackage ./pkgs/mpvScripts/autodeint {};
                autosub = pkgs.callPackage ./pkgs/mpvScripts/autosub {};
                # NOTE: Has no license?
                reload = pkgs.callPackage ./pkgs/mpvScripts/reload {};
              };

              # texlive
              texlive = {
                mla-tex = pkgs.callPackage ./pkgs/texlive/mla-tex {};
              };

              # vimPlugins
              vimPlugins = {
                ingo-library = pkgs.callPackage ./pkgs/vimPlugins/ingo-library {};
                IndentTab = pkgs.callPackage ./pkgs/vimPlugins/IndentTab {};
                vim-evanesco = pkgs.callPackage ./pkgs/vimPlugins/vim-evanesco {};
              };

              # 3ds
              save3ds = pkgs.callPackage ./pkgs/save3ds {};
              custom-install = upkgs.callPackage ./pkgs/custom-install {
                inherit save3ds;
              };

              # switch
              nxci = pkgs.callPackage ./pkgs/4nxci {};
              ns-usbloader = pkgs.callPackage ./pkgs/ns-usbloader {};
              nsz = pkgs.callPackage ./pkgs/nsz {};

              # zsh
              zsh-git-escape-magic = pkgs.callPackage ./pkgs/zsh-git-escape-magic {};
              zsh-manydots-magic = pkgs.callPackage ./pkgs/zsh-manydots-magic {};

              bitwarden-rofi = pkgs.callPackage ./pkgs/bitwarden-rofi {};
              jqq = pkgs.callPackage ./pkgs/jqq {};
              rural = pkgs.callPackage ./pkgs/rural {};

              # TODO: Once rust stable is >= 1.57 switch to pkgs
              toluol = upkgs.callPackage ./pkgs/toluol {};
            };
          };
        }

        homeage.homeManagerModules.homeage

        # Shared modules
        ./nix-overlays
        ./general
        ./programs
        ./secrets
        ./services

        # Import this machine's config
        props.files."home.nix"

        {
          programs.home-manager.enable = true;

	  home = {
	    inherit username stateVersion;
            homeDirectory = "/home/${username}";
            enableNixpkgsReleaseCheck = true;
	  };
	}
      ];
    };

    homeConfigurationsRaw = mapAttrs mkConfig (filterAttrs (_: props: hasAttr "home.nix" props.files) machines);
  in {
    devShell.x86_64-linux = let
      pkgs = nixpkgs.legacyPackages.x86_64-linux;
    in pkgs.mkShell {
      shellHook = /* ft=sh */ ''
        nix build ".#homeConfigurations.\"$(whoami)@$(hostname)\".activationPackage"
        [[ -e "./result" ]] && ./result/activate
        [[ -e "./result" ]] && rm result
      '';
    };

    homeConfigurations = mapAttrs (const home-manager.lib.homeManagerConfiguration) homeConfigurationsRaw;
  };
}
