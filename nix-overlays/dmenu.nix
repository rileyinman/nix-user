final: prev:
{
  dmenu = final.writeShellScriptBin
    "dmenu"
    "exec ${prev.rofi}/bin/rofi -dmenu $@";

  dmenu-wayland = final.writeShellScriptBin
    "dmenu-wl"
    "exec ${prev.unstable.rofi-wayland}/bin/rofi -dmenu $@";
}
