_: prev:
{
  iosevka = prev.iosevka.override {
    set = "custom";

    privateBuildPlan = {
      family = "Iosevka";

      variants = {
        design = {
          capital-q = "crossing";
          capital-z = "straight-serifless-with-horizontal-crossbar";
          a = "single-storey-earless-corner-tailed";
          z = "straight-serifless-with-horizontal-crossbar";
          two = "straight-neck";
          three = "flat-top";
          four = "open-non-crossing";
          seven = "straight-crossbar";
          asterisk = "hex-high";
          number-sign = "slanted";
          question = "corner-flat-hooked";
        };

        italic = {
          capital-a = "curly-serifless";
          capital-b = "more-asymmetric-serifless";
          capital-d = "more-rounded-serifless";
          capital-g = "toothless-rounded-serifless-hooked";
          capital-i = "serifless";
          capital-j = "serifless";
          capital-k = "curly-serifless";
          capital-n = "asymmetric";
          capital-q = "curly-tailed";
          capital-r = "standing";
          capital-v = "curly";
          capital-w = "curly";
          capital-x = "curly-serifless";
          capital-y = "curly-serifless";
          capital-z = "curly-serifless-with-horizontal-crossbar";
          v = "curly";
          w = "curly";
          x = "curly-serifless";
          z = "curly-serifless-with-horizontal-crossbar";
          zero = "slashed-oval";
          two = "curly-neck";

          # THESE TWO BROKE
          # five = "oblique-upper-left-bar";
          # six = "open-contour";

          seven = "curly-crossbar";
          eight = "two-circles";
          nine = "open-contour";
          number-sign = "upright";
          question = "smooth";
        };

        oblique.number-sign = "upright";
      };

      ligations.inherits = "clike";
    };
  };
}
