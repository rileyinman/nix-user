{ lib, nixpkgs-unstable, nur, rust-overlay, ... }:

{
  nixpkgs.overlays = with lib; mkMerge [
    (mkOrder 0 [(
      _: prev:
      {
        unstable = import nixpkgs-unstable {
          inherit (prev) config;
          localSystem = prev.system;
          overlays = [];
        };
      }
    )])

    [
      rust-overlay.overlay
      nur.overlay
      ( import ./dmenu.nix )
      ( import ./dvd.nix )
      ( import ./fonts.nix )
    ]
  ];
}
