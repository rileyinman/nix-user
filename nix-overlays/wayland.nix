_: prev:
{
  pass = prev.pass-wayland;

  # One of the rofi devs refuses to merge wayland support. Cool!
  rofi = prev.unstable.rofi-wayland;

  xclip = prev.unstable.wl-clipboard-x11;
  xsel = prev.unstable.wl-clipboard-x11;
}
