{ stdenv, fetchurl, makeWrapper
, gsettings-desktop-schemas, gtk3, jdk11 }:

stdenv.mkDerivation rec {
  pname = "ns-usbloader";
  version = "5.2";

  src = fetchurl {
    url = "https://github.com/developersu/${pname}/releases/download/v${version}/ns-usbloader-${version}.jar";
    sha256 = "06kzshlvqfwcjjddzqqgq13pqa5qjlajpyn6ksqxy5p5hgarj6i6";
  };

  dontUnpack = true;

  nativeBuildInputs = [ makeWrapper ];

  dontConfigure = true;
  dontBuild = true;

  installPhase = ''
    mkdir -p $out/{bin,opt}
    cp $src $out/opt/ns-usbloader-${version}.jar

    makeWrapper ${jdk11}/bin/java $out/bin/ns-usbloader \
      ''${makeWrapperArgs[@]} \
      --prefix XDG_DATA_DIRS : "${gsettings-desktop-schemas}/share/gsettings-schemas/${gsettings-desktop-schemas.name}:${gtk3}/share/gsettings-schemas/${gtk3.name}" \
      --add-flags "-jar $out/opt/ns-usbloader-${version}.jar"
  '';
}
