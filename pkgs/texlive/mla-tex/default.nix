{ fetchFromGitHub, lib, stdenv }:

stdenv.mkDerivation rec {
  pname = "mla-tex";
  version = "1.2";
  tlType = "run";

  src = fetchFromGitHub {
    owner = "raxod502";
    repo = "mla-tex";
    rev = "v${version}";
    sha256 = "0xi58a6bah0ym51ngf1mfk5rkdq82nc89gyxi8iavmnhi1ydmmcd";
  };

  dontConfigure = true;
  dontBuild = true;

  patches = [ ./fix-tinos.patch ];

  installPhase = ''
    mkdir -p $out/tex
    cp mla.cls $out/tex/
  '';

  meta = {
    description = "Typeset your MLA papers in XeTeX";
    license = lib.licenses.mit;
    homepage = https://github.com/raxod502/mla-tex;
  };
}
