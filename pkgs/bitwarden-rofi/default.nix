{ lib, stdenv
, fetchFromGitHub, makeWrapper
, bitwarden-cli
, jq
, keyutils
, rofi
, unixtools
, wl-clipboard
, ydotool }:

stdenv.mkDerivation rec {
  pname = "bitwarden-rofi";
  version = "2021-3-17";

  src = fetchFromGitHub {
    owner = "mattydebie";
    repo = pname;
    rev = "62c95afd5634234bac75855dc705d4da5f4fab69";
    hash = "sha256-gv18H+J2pjT6d4qoLTcxUeo4r1xzXhBsOoFFpvl3Deo=";
  };

  nativeBuildInputs = [ makeWrapper ];

  binPath = lib.makeBinPath ([
    bitwarden-cli
    jq
    keyutils
    rofi
    unixtools.getopt
    wl-clipboard
    ydotool
  ]);

  installPhase = ''
    mkdir -p $out/{bin,opt/$pname}

    install -Dm755 "bwmenu" "$out/opt/$pname/"
    install -Dm755 "lib-bwmenu" "$out/opt/$pname/"
    ln -s "$out/opt/$pname/bwmenu" "$out/bin/"

    mkdir -p $out/usr/share/doc/$pname/img
    install -Dm644 "README.md" "$out/usr/share/doc/$pname/"
    install -Dm644 img/* "$out/usr/share/doc/$pname/img/"

    wrapProgram "$out/bin/bwmenu" --prefix PATH : ${binPath}
  '';
}
