{ fetchFromGitHub, rustPlatform
, openssl, pkg-config }:

rustPlatform.buildRustPackage rec {
  pname = "rural";
  version = "2019-08-29";

  src = fetchFromGitHub {
    owner = "saghm";
    repo = pname;
    rev = "616c506e2d1620e5850e1f125a751b23bb823920";
    hash = "sha256-pYpeFl6dzViRds6xL8d36iTf5nVeiFb/G6Px8HJlC/k=";
  };

  cargoHash = "sha256-xcgMbcmyL8cCmgKBs5FtKWYoxFxKwNh3U11NngWj0n8=";

  nativeBuildInputs = [ pkg-config ];

  buildInputs = [ openssl.dev ];

  doCheck = false;
}
