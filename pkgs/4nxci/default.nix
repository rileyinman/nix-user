{ fetchFromGitHub, stdenv }:

stdenv.mkDerivation rec {
  pname = "4nxci";
  version = "4.03";

  src = fetchFromGitHub {
    owner = "The-4n";
    repo = "4NXCI";
    rev = "v${version}";
    sha256 = "0n49sqv6s8cj2dw1dbcyskfc2zr92p27f1bdd6jqfbawv0fqr1wf";
  };

  preBuild = ''
    mv config.mk.template config.mk
  '';

  installPhase = ''
    mkdir -p $out/bin/
    cp 4nxci $out/bin/
  '';
}
