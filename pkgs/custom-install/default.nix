{ fetchFromGitHub, python3Packages, save3ds }:

python3Packages.buildPythonPackage rec {
  pname = "custom-install";
  version = "2.1";

  format = "other";

  src = fetchFromGitHub {
    owner = "ihaveamac";
    repo = pname;
    rev = "v${version}";
    hash = "sha256-Po+8kuyhxuGA0M2rN4YMBiOnK8/uUjqiHF6f0dDYCTM=";
  };

  buildInputs = [ save3ds ];
  pythonLibs = with python3Packages; [
    events
    pyctr
    tkinter
  ];

  dontBuild = true;
  dontConfigure = true;
  doCheck = false;

  installPhase = ''
    mkdir -p $out/{bin,opt}
    mkdir -p $out/opt/bin/linux
    ln -s ${save3ds}/bin/save3ds_fuse $out/opt/bin/linux/

    cp -r $src/{bin,extras,finalize,TaskbarLib.tlb,ci-gui.py,custominstall.py} $out/opt/

    ln -s $out/opt/ci-gui.py $out/bin/ci-gui
    ln -s $out/opt/custominstall.py $out/bin/custom-install
    chmod +x $out/bin/{ci-gui,custom-install}

    cd $out/opt
    patch -p1 < ${./shebang.patch}

    wrapProgram $out/bin/ci-gui --prefix PYTHONPATH : ${python3Packages.makePythonPath pythonLibs}
    wrapProgram $out/bin/custom-install --prefix PYTHONPATH : ${python3Packages.makePythonPath pythonLibs}
  '';
}
