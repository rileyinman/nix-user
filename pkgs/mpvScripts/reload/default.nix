{ fetchFromGitHub, lib, stdenvNoCC }:

stdenvNoCC.mkDerivation rec {
  pname = "mpv-reload";
  version = "2022-01-27";

  src = fetchFromGitHub {
    owner = "4e6";
    repo = pname;
    rev = "c1219b6ac3ee3de887e6a36ae41a8e478835ae92";
    hash = "sha256-+DoKPIulQA3VSeXo8DjoxnPwDfcuCO5YHpXmB+M7EWk=";
  };

  installPhase = ''
    mkdir -p $out/share/mpv/scripts
    cp reload.lua $out/share/mpv/scripts/
  '';

  passthru.scriptName = "reload.lua";

  meta = with lib; {
    description = "MPV plugin for automatic reloading of slow/stuck video streams";
    homepage = "https://github.com/4e6/mpv-reload";
  };
}
