{ fetchFromGitHub, lib, stdenvNoCC
, python3Packages }:

stdenvNoCC.mkDerivation rec {
  pname = "mpv-autosub";
  version = "2021-06-29";

  src = fetchFromGitHub {
    owner = "davidde";
    repo = pname;
    rev = "35115355bd339681f97d067538356c29e5b14afa";
    hash = "sha256-BKT/Tzwl5ZA4fbdc/cxz0+CYc1zyY/KOXc58x5GYow0=";
  };

  dontBuild = true;

  postPatch = ''
    substituteInPlace autosub.lua \
      --replace "/home/david/.local/bin/subliminal" "${python3Packages.subliminal}/bin/subliminal"
  '';

  installPhase = ''
    mkdir -p $out/share/mpv/scripts
    cp autosub.lua $out/share/mpv/scripts/
  '';

  passthru.scriptName = "autosub.lua";

  meta = with lib; {
    description = "Fully automatic subtitle downloading for the MPV media player";
    license = licenses.mit;
    homepage = "https://github.com/davidde/mpv-autosub";
  };
}
