{ fetchFromGitHub, lib, stdenvNoCC }:

stdenvNoCC.mkDerivation rec {
  pname = "zsh-manydots-magic";
  version = "2017-06-19";

  src = fetchFromGitHub {
    owner = "knu";
    repo = pname;
    rev = "4372de0718714046f0c7ef87b43fc0a598896af6";
    hash = "sha256-lv7e7+KBR/nxC43H0uvphLcI7fALPvxPSGEmBn0g8HQ=";
  };

  dontConfigure = true;
  dontBuild = true;

  installPhase = ''
    install -Dm0644 manydots-magic $out/share/zsh-manydots-magic
  '';

  meta = with lib; {
    description = "ZLE tweak for emulating `...'==`../..' etc";
    homepage = "https://github.com/knu/zsh-manydots-magic/";
    license = licenses.unfreeRedistributable;
  };
}
