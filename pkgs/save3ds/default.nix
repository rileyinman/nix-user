{ fetchFromGitHub, rustPlatform, lib, stdenv
, fuse, pkg-config }:

rustPlatform.buildRustPackage rec {
  pname = "save3ds";
  version = "2020-06-16";

  src = fetchFromGitHub {
    owner = "wwylele";
    repo = "save3ds";
    rev = "568b0597b17da0c8cfbd345bab27176cd84bd883";
    hash = "sha256-IZjbe8Cd1YQfLXjxxGVSM1CyUKKYhHwqmARX7mnTHSw=";
  };

  nativeBuildInputs = [ pkg-config ];
  buildInputs = [ fuse ];

  cargoHash = "sha256-5/bG3fny686bSomgTPfs2IWO+I6R2MGMIWtRshBS+tM=";

  doCheck = false;

  meta = {
    description = "Extract, import and FUSE program for common save format for 3DS, written in rust.";
    license = lib.licenses.mit;
    homepage = "https://github.com/wwylele/save3ds";
  };
}
