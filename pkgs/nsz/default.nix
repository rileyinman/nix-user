{ python3Packages, fetchFromGitHub }:

python3Packages.buildPythonPackage rec {
  pname = "nsz";
  version = "4.0.1";

  src = fetchFromGitHub {
    owner = "nicoboss";
    repo = pname;
    rev = version;
    sha256 = "1l8c6wz0i8xlpjiay9p1xa0a79ykp0sl0fb55qr8zcn9vmaz0qjr";
  };

  propagatedBuildInputs = with python3Packages; [
    enlighten
    pycryptodome
    zstandard
  ];

  doCheck = false;
}
