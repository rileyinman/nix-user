{ fetchgit, rustPlatform }:

rustPlatform.buildRustPackage rec {
  pname = "toluol";
  version = "2022-03-10";

  src = fetchgit {
    url = "https://git.sr.ht/~mvforell/${pname}";
    rev = "085fd25e3b6c0a557b5e741aeb3f271aefcd40c2";
    hash = "sha256-SbR8OtfavzR+K0fkamgJW9Z4BcXjOpkss7Gkgq6VjfQ=";
  };

  cargoHash = "sha256-fhXXOIPo6ujjQ2y9AxvZ2vRdWlvPAtpVYCtpBXGCC2w=";
}
