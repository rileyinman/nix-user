{ fetchFromGitHub, lib, stdenvNoCC }:

stdenvNoCC.mkDerivation rec {
  pname = "zsh-git-escape-magic";
  version = "2014-11-05";

  src = fetchFromGitHub {
    owner = "knu";
    repo = pname;
    rev = "62af4f6a66601a517e168039614e5b528741a844";
    hash = "sha256-punqAOYJkFGHB0e0MgYxzkWitv6dKQWBOoOWRqeJB7U=";
  };

  dontConfigure = true;
  dontBuild = true;

  installPhase = ''
    install -Dm0644 git-escape-magic $out/share/zsh-git-escape-magic
  '';

  meta = with lib; {
    description = "ZLE tweak for git command line arguments";
    homepage = "https://github.com/knu/zsh-git-escape-magic/";
    license = licenses.unfreeRedistributable;
  };
}
