{ fetchFromGitHub, vimUtils }:

vimUtils.buildVimPlugin rec {
  pname = "vim-evanesco";
  version = "2019-06-19";

  src = fetchFromGitHub {
    owner = "pgdouyon";
    repo = "vim-evanesco";
    rev = "eef359f289cf3b4dffb91647510b2d6def080c9e";
    sha256 = "1jzm15ssnh81cki465qd220dv0xcxjm184r8fizmah597bq79w2y";
  };
}
