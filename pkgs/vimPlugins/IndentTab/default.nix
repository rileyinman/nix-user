{ fetchFromGitHub, vimUtils }:

vimUtils.buildVimPlugin rec {
  pname = "IndentTab";
  version = "1.10";

  src = fetchFromGitHub {
    owner = "vim-scripts";
    repo = "IndentTab";
    rev = version;
    sha256 = "1sx30rj8dsrv08rs7q9b068mxnmfxlzrvlpm8jiif08xf08najg0";
  };
}
