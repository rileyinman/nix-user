{ fetchFromGitHub, vimUtils }:

vimUtils.buildVimPlugin rec {
  pname = "ingo-library";
  version = "1.024";

  src = fetchFromGitHub {
    owner = "vim-scripts";
    repo = "ingo-library";
    rev = version;
    sha256 = "0n06ri6icjlhw06rc6sjirpm4dfs3lyc8brm9igx848zqsv83p4v";
  };
}
