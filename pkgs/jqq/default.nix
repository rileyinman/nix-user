{ lib, stdenv, bundlerEnv, fetchFromGitHub, ruby, makeWrapper, writeShellScript }:

stdenv.mkDerivation rec {
  pname = "jqq";
  version = "2020-05-29";

  src = fetchFromGitHub {
    owner = "jcsalterego";
    repo = pname;
    rev = "da0862428e6ceee695fceb2253da12fc79ec99d9";
    hash = "sha256-iDEinxc+uulfEianCaFwm5D74f9KQbY49hPf3nrasHA=";
  };

  nativeBuildInputs = [ makeWrapper ];

  buildInputs = [ ruby ];

  phases = [ "installPhase" ];

  installPhase = let
    env = bundlerEnv {
      name = "${pname}-${version}-gems";
      inherit ruby;
      gemdir = ./.;
    };
  in ''
    mkdir -p $out/{bin,opt}
    cp $src/${pname}.rb $out/opt/
    makeWrapper ${env}/bin/bundle $out/bin/${pname} \
      ''${makeWrapperArgs[@]} \
      --add-flags "exec ${ruby}/bin/ruby $out/opt/${pname}.rb"
  '';
}
