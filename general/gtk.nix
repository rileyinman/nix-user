{ pkgs, upkgs, ... }:

let
  commonConfig = {
    gtk-application-prefer-dark-theme = true;
    gtk-cursor-theme-name = "capitaine-cursors";
    gtk-cursor-theme-size = 0;
    gtk-fallback-icon-theme = "hicolor";
    gtk-toolbar-style = "GTK_TOOLBAR_ICONS";
    gtk-toolbar-icon-size = "GTK_ICON_SIZE_LARGE_TOOLBAR";
    gtk-button-images = 1;
    gtk-menu-images = 1;
    gtk-enable-event-sounds = 0;
    gtk-enable-input-feedback-sounds = 0;
    gtk-xft-antialias = 1;
    gtk-xft-hinting = 1;
    gtk-xft-hintstyle = "hintfull";
    gtk-xft-rgba = "rgb";
  };
in
{
  home.packages = [
    upkgs.capitaine-cursors
    pkgs.dconf
    pkgs.hicolor-icon-theme
  ];

  home.file.".icons/default/index.theme".text = /* ft=dosini */ ''
    [Icon Theme]
    Name=Default
    Comment=Default Cursor Theme
    Inherits=capitaine-cursors
  '';

  gtk = {
    enable = true;

    iconTheme = {
      name = "Papirus-Dark";
      package = pkgs.papirus-icon-theme;
    };

    theme = {
      name = "Arc-Dark";
      package = pkgs.arc-theme;
    };

    gtk2.extraConfig = /* ft=gtkrc */ ''
      gtk-cursor-theme-name="capitaine-cursors"
      gtk-cursor-theme-size=0
      gtk-toolbar-style=GTK_TOOLBAR_ICONS
      gtk-toolbar-icon-size=GTK_ICON_SIZE_LARGE_TOOLBAR
      gtk-button-images=1
      gtk-menu-images=1
      gtk-enable-event-sounds=0
      gtk-enable-input-feedback-sounds=0
      gtk-xft-antialias=1
      gtk-xft-hinting=1
      gtk-xft-hintstyle="hintfull"
      gtk-xft-rgba="rgb"
    '';

    gtk3.extraConfig = commonConfig;
    # gtk4.extraConfig = commonConfig;
  };
}
