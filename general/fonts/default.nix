{ pkgs, unfreepkgs, ... }:

{
  home.packages = with pkgs; [
    unfreepkgs.aegyptus
    font-awesome_5
    iosevka
    liberation_ttf
    noto-fonts-emoji-blob-bin
    sarasa-gothic
    source-sans-pro
    ttf_bitstream_vera
  ];

  fonts.fontconfig.enable = true;

  xdg.configFile."fontconfig/conf.d" = {
    source = ./fontconfig;
    recursive = true;
  };

  xdg.configFile."fontconfig/conf.d/45-generic.conf".source = pkgs.fetchurl {
    url = "https://gitlab.freedesktop.org/fontconfig/fontconfig/-/raw/main/conf.d/45-generic.conf";
    hash = "sha256-JN6faBidVEu0zAhNm9jzpZ2LmIqdxEsR6Yd9Wt1fyUg=";
  };

  xdg.configFile."fontconfig/conf.d/60-generic.conf".source = pkgs.fetchurl {
    url = "https://gitlab.freedesktop.org/fontconfig/fontconfig/-/raw/main/conf.d/60-generic.conf";
    hash = "sha256-GVvIYF3SMonULBDXnME6XOSks8+WXfWzDswLcUm7lR8=";
  };
}
