{ pkgs, ... }:

{
  imports = [ ../services/easyeffects.nix ];
  home.packages = with pkgs; [
    helvum
    pavucontrol
  ];
}
