{ ... }:

{
  nixpkgs.config = import ../config.nix;

  # TODO: https://github.com/nix-community/home-manager/pull/2718/
  # nix.registry = {
  #   nixpkgs.flake = nixpkgs;
  # };
}
