{ pkgs, upkgs, ... }:

{
  home.packages = with pkgs; [
    dvdplusrwtools
    upkgs.rubyripper
    libdvdread
    libdvdnav
    libdvdcss
  ];
}
