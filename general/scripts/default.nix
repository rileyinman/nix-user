{ pkgs, ... }:

{
  home.packages = [(pkgs.buildEnv {
    name = "home-scripts";
    paths = [(pkgs.runCommand "home-scripts" {} /* ft=sh */ ''
      mkdir -p $out/bin/
      cp ${./files}/* $out/bin/
    '')];
  })];
}
