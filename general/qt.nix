{ pkgs, ... }:

{
  home.packages = with pkgs; [
    qt5ct
    libsForQt5.qtstyleplugins
  ];

  home.sessionVariables.QT_QPA_PLATFORMTHEME = "qt5ct";

  xdg.configFile."qt5ct/qt5ct.conf".text = /* ft=dosini */ ''
    [Appearance]
    color_scheme_path=${pkgs.qt5ct}/share/qt5ct/colors/airy.conf
    custom_palette=false
    icon_theme=Papirus-Dark
    standard_dialogs=default
    style=gtk2
  '';
}
