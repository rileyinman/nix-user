{ ... }:

{
  imports = [
    ./compose
    ./fonts
    ./gtk.nix
    ./ibus.nix
    ./qt.nix
  ];
}
