{ ... }:

{
  imports = [
    ./man.nix
    ./nix.nix
    ./scripts
    ./systemd.nix
    ./xdg.nix
  ];
}
