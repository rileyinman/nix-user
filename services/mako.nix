{ pkgs, ... }:

{
  home.packages = [ pkgs.libnotify ];

  services.mako = {
    enable = true;
    anchor = "bottom-right";
    backgroundColor = "#2f343f";
    borderColor = "#262b36";
    font = "sans 11";
    margin = "15";
    output = "eDP-1";
  };
}
