{ pkgs, ... }:

{
  home.packages = [ pkgs.libnotify ];

  services.dunst = {
    enable = true;
    iconTheme = {
      package = pkgs.papirus-icon-theme;
      name = "Papirus-Dark";
      size = "16x16";
    };

    settings = {
      global = {
        monitor = 0;
        follow = "mouse";
        geometry = "0x0-10-10";
        indicate_hidden = true;
        shrink = true;
        padding = 8;
        horizontal_padding = 15;
        sort = true;
        idle_threshold = 120;
        font = "sans 11";
        markup = "full";
        format = "%s %p\\n%b";
        alignment = "center";
        show_age_threshold = 60;
        word_wrap = true;
        icon_position = "left";
        max_icon_size = 64;
      };

      urgency_low = {
        background = "#2f343f";
        foreground = "#ffffff";
        timeout = 0;
      };

      # urgency_normal = urgency_low;
      # urgency_critical = urgency_low;
    };
  };
}
