{ pkgs, ... }:

{
  systemd.user.services.ssh-agent = {
    Unit.Description = "SSH key agent";

    Service = {
      Type = "simple";
      Environment = "SSH_AUTH_SOCK=%t/ssh-agent.socket";
      ExecStart = "${pkgs.openssh}/bin/ssh-agent -D -a $SSH_AUTH_SOCK";
    };

    Install.WantedBy = [ "default.target" ];
  };

  home.sessionVariables.SSH_AUTH_SOCK = "$XDG_RUNTIME_DIR/ssh-agent.socket";
}
