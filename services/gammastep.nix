{ ... }:

{
  services.gammastep = {
    enable = true;
    provider = "geoclue2";
    temperature.day = 5500;
    temperature.night = 4800;
  };
}
